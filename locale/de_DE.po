# 
msgid ""
msgstr "Content-Type: text/plain; charset=utf-8\n"

msgctxt "field:incoterm.category,name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:incoterm.category,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:incoterm.category,suffix:0"
msgid "Incoterm Suffix"
msgstr "Incoterm Suffix"

msgctxt "field:incoterm.incoterm,active:0"
msgid "Active"
msgstr "Aktiv"

msgctxt "field:incoterm.incoterm,category:0"
msgid "Category"
msgstr "Kategorie"

msgctxt "field:incoterm.incoterm,code:0"
msgid "Code"
msgstr "Code"

msgctxt "field:incoterm.incoterm,name:0"
msgid "Name"
msgstr "Name"

msgctxt "field:incoterm.incoterm,rec_name:0"
msgid "Name"
msgstr "Name"

msgctxt "model:incoterm.category,name:0"
msgid "Incoterm Category"
msgstr "Kategorie Incoterm"

msgctxt "model:incoterm.category,name:incoterm_2010"
msgid "Incoterms 2010"
msgstr "Incoterms 2010"

msgctxt "model:incoterm.category,suffix:incoterm_2010"
msgid "(as per INCOTERMS 2010)"
msgstr "(gemäß INCOTERMS 2010)"

msgctxt "model:incoterm.incoterm,name:0"
msgid "Incoterm"
msgstr "Lieferbedingung"

msgctxt "model:incoterm.incoterm,name:incoterm_cfr"
msgid "cost and freight"
msgstr "Kosten und Fracht"

msgctxt "model:incoterm.incoterm,name:incoterm_cif"
msgid "cost, insurance and freight"
msgstr "Kosten, Versicherung und Fracht"

msgctxt "model:incoterm.incoterm,name:incoterm_cip"
msgid "carriage and insurance paid to"
msgstr "Frachtfrei versichert"

msgctxt "model:incoterm.incoterm,name:incoterm_cpt"
msgid "carriage paid to"
msgstr "Frachtfrei"

msgctxt "model:incoterm.incoterm,name:incoterm_dap"
msgid "delivered at place"
msgstr "Geliefert"

msgctxt "model:incoterm.incoterm,name:incoterm_dat"
msgid "delivered at terminal"
msgstr "Geliefert an Terminal"

msgctxt "model:incoterm.incoterm,name:incoterm_ddp"
msgid "delivered duty paid"
msgstr "Geliefert verzollt"

msgctxt "model:incoterm.incoterm,name:incoterm_exw"
msgid "ex works"
msgstr "Ab Werk"

msgctxt "model:incoterm.incoterm,name:incoterm_fas"
msgid "free alongside ship"
msgstr "Frei Längsseite Schiff"

msgctxt "model:incoterm.incoterm,name:incoterm_fca"
msgid "free carrier"
msgstr "Frei Frachtführer"

msgctxt "model:incoterm.incoterm,name:incoterm_fob"
msgid "free on board"
msgstr "Frei an Bord"

msgctxt "model:ir.action,name:act_incoterm_form"
msgid "Incoterms"
msgstr "Lieferbedingungen"

msgctxt "model:ir.ui.menu,name:menu_incoterm"
msgid "Incoterms"
msgstr "Lieferbedingungen"

msgctxt "model:ir.ui.menu,name:menu_incoterm_form"
msgid "Incoterms"
msgstr "Lieferbedingungen"

msgctxt "view:incoterm.category:0"
msgid "Incoterm Categories"
msgstr "Kategorien Incoterms"

msgctxt "view:incoterm.incoterm:0"
msgid "Incoterm"
msgstr "Lieferbedingung"

msgctxt "view:incoterm.incoterm:0"
msgid "Incoterm Category"
msgstr "Kategorie Incoterm"

msgctxt "view:incoterm.incoterm:0"
msgid "Incoterms"
msgstr "Lieferbedingungen"
