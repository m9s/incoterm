#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Category(ModelSQL, ModelView):
    'Incoterm Category'
    _name = 'incoterm.category'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')

Category()


class Incoterm(ModelSQL, ModelView):
    'Incoterm'
    _name = 'incoterm.incoterm'
    _description = __doc__

    name = fields.Char('Name', required=True, translate=True, loading='lazy')
    code = fields.Char('Code', required=True)
    active = fields.Boolean('Active', select=1)
    category = fields.Many2One('incoterm.category', 'Category')

    def default_active(self):
        return True

    def get_rec_name(self, ids, name):
        res = {}
        if not ids:
            return res
        for incoterm in self.browse(ids):
            res[incoterm.id] = " - ".join(x for x in [
                    incoterm.code, incoterm.name] if x)
        return res

Incoterm()
