# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
{
    'name': 'Incoterm',
    'name_de_DE': 'Incoterm',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds Incoterms and Incoterm categories
    Incoterms or International Commercial terms are a series
    of international sales with terms, published by International Chamber of
    Commerce (ICC) and widely used in international commercial transactions
    ''',
    'description_de_DE': '''
    - Fügt Incoterms gestaffelt nach Incoterm Kategorien hinzu
    Incoterms oder International Commercial terms sind eine Sammlung von
    Bestimmungen, die durch die Internationale Handelskammer (International
    Chamber of Commerce (ICC)) publiziert werden und Anwendung im
    internationalen Warenaustausch finden.
    ''',
    'depends': [
        'ir',
        'res'
    ],
    'xml': [
        'incoterm.xml'
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
